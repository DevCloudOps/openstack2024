echo apt update
echo apt list --upgradable
echo apt upgrade
# echo apt install docker.io
echo from https://docs.openstack.org/install-guide/environment-packages-ubuntu.html
echo
echo add-apt-repository cloud-archive:bobcat
echo
echo apt install chrony mariadb-server python3-pymysql rabbitmq-server memcached python3-memcache etcd
echo
echo minimal openstack services
echo
echo from https://docs.openstack.org/neutron/2023.2/install/controller-install-option2-ubuntu.html
echo
echo apt install python3-openstackclient keystone nova-compute
echo
echo self service network -- Install and configure the Networking components on the controller node.
echo apt install neutron-server neutron-plugin-ml2 neutron-openvswitch-agent neutron-l3-agent neutron-dhcp-agent neutron-metadata-agent
echo
echo https://docs.openstack.org/neutron/2023.2/install/ovn/manual_install.html
echo 'Ubuntu/Debian includes ovn-central, ovn-host, ovn-docker, and ovn-common packages that pull in the appropriate Open vSwitch dependencies as needed.'
