#!/bin/bash
echo 'wholesaleinternet.net host seems to show to ethernet link cards: ens9 and enp12s0'
echo 'seems ens9 is the primary ... but is enp12s0 available for openstack conf?'
echo "ip a | egrep 'ens|enp'"
echo " ... attempt to startup link enp12s0 and sleep 1 ..."
echo 'ip link set dev enp12s0 up && sleep 1'
ip link set dev enp12s0 up && sleep 1
# sleep another sec?
sleep 1
echo 
ip a | egrep 'ens|enp'
echo 
echo 'ok shut down enp12s0'
echo 
ip link set dev enp12s0 down
ip a|egrep 'ens|enp'
