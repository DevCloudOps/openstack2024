# openstack install guides for ubuntu

baremetal hosts: helicit.us wehike.us 1percent.us

## openstack bobcat release install guides for ubuntu 22

https://docs.openstack.org/install-guide/index.html

https://docs.openstack.org/install-guide/environment-security.html

https://docs.openstack.org/install-guide/environment-networking-controller.html

https://docs.openstack.org/install-guide/environment-networking-compute.html

https://docs.openstack.org/install-guide/environment-networking-storage-cinder.html

https://docs.openstack.org/install-guide/environment-networking-verify.html

https://docs.openstack.org/install-guide/environment-ntp.html

https://docs.openstack.org/install-guide/environment-ntp-verify.html

https://docs.openstack.org/install-guide/environment-packages-ubuntu.html

https://docs.openstack.org/install-guide/environment-sql-database-ubuntu.html

https://docs.openstack.org/install-guide/environment-messaging-ubuntu.html

https://docs.openstack.org/install-guide/environment-memcached-ubuntu.html

https://docs.openstack.org/install-guide/environment-etcd-ubuntu.html

https://docs.openstack.org/install-guide/openstack-services.html

## minimal bobcat install:

https://docs.openstack.org/keystone/2023.2/install/

https://docs.openstack.org/glance/2023.2/install/

https://docs.openstack.org/placement/2023.2/install/

https://docs.openstack.org/nova/2023.2/install/

https://docs.openstack.org/neutron/2023.2/install/

https://docs.openstack.org/horizon/2023.2/install/

https://docs.openstack.org/cinder/2023.2/install/

